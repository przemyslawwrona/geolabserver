package pl.geolab.marshallers.project

import grails.converters.JSON
import pl.geolab.AppReport
import pl.geolab.marshallers.Marshaller

class ReportMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(AppReport) { AppReport report ->
            Map<String, String> map = [:]
            map['id'] = report.id
            map['dateCreated'] = report.dateCreated
            map['lastUpdated'] = report.lastUpdated
            map['classType'] = report.class.getSimpleName()
            return map
        }
    }
}
