package pl.geolab.marshallers.project

import grails.converters.JSON
import pl.geolab.AppProject
import pl.geolab.marshallers.Marshaller

class ProjectMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(AppProject) { AppProject project ->
            Map<String, String> map = [:]
            map['id'] = project.id
            map['dateCreated'] = project.dateCreated
            map['lastUpdated'] = project.lastUpdated
            map['title'] = project.title
            map['localization'] = project.localization
            map['description'] = project.description
            return map
        }
    }
}
