package pl.geolab.marshallers.gravimetry

import geo.gravimetry.dryft.ObservationDryft
import grails.converters.JSON
import pl.geolab.marshallers.Marshaller

class GravObservationDryftMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(ObservationDryft) { ObservationDryft observationDryft ->
            ObservationDryft
            Map<String, String> map = [:]
            map['name'] = observationDryft.observation.name
            map['gRef'] = observationDryft.observation.getGRef()
            map['dryft'] = observationDryft.dryft
            map['gRefPopra'] = observationDryft.getGRefPoprawione()
            map['deltaG'] = observationDryft.getDeltaG()
            return map
        }
    }
}
