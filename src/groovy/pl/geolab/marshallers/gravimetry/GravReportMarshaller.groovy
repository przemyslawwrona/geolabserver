package pl.geolab.marshallers.gravimetry

import geo.gravimetry.report.Report
import geo.gravimetry.report.ReportResultable
import grails.converters.JSON
import pl.geolab.marshallers.Marshaller

class GravReportMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(Report) { Report report ->
            Map<String, String> map = [:]
            map['gravimeter'] = report.gravimeter
            map['observations'] = report.observations
            map['corrections'] = report.corrections
            map['dryfts'] = report.dryfts

            if (report instanceof ReportResultable) {
                map['deltaG'] = report.getDeltaG()
            }

            return map
        }
    }
}
