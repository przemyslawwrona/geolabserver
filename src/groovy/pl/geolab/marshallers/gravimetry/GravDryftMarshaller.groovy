package pl.geolab.marshallers.gravimetry

import geo.gravimetry.dryft.Dryft
import grails.converters.JSON
import pl.geolab.marshallers.Marshaller

class GravDryftMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(Dryft) { Dryft dryft ->
            Map<String, String> map = [:]
            map['observations'] = dryft.corrections
            map['dryft'] = dryft.dryft
            return map
        }
    }
}
