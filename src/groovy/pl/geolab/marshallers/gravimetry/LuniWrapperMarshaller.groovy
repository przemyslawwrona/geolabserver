package pl.geolab.marshallers.gravimetry

import grails.converters.JSON
import pl.geolab.marshallers.Marshaller
import pl.geolab.wrapper.LuniWrapper

class LuniWrapperMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(LuniWrapper) { LuniWrapper luniWrapper ->
            Map<String, String> map = [:]
            map['date'] = luniWrapper.date
            map['value'] = luniWrapper.value
            return map
        }
    }
}
