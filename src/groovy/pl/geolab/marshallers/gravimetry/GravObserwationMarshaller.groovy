package pl.geolab.marshallers.gravimetry

import geo.gravimetry.Observation
import grails.converters.JSON
import org.joda.time.DateTime
import pl.geolab.marshallers.Marshaller

class GravObserwationMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(Observation) { Observation observation ->
            Observation
            Map<String, String> map = [:]
            map['name'] = observation.name

            map['latitude'] = observation.latitude
            map['longitude'] = observation.longitude
            map['height'] = observation.height
            map['date'] = observation.dateTime.toDate()
            map['value'] = observation.value
            map['instrumentHeight'] = observation.instrumentHeight
            map['gradient'] = observation.gradient
            map['pressure'] = observation.pressure

            map['g'] = observation.g
            map['corrections'] = observation.corrections
            map['gRef'] = observation.getGRef()

            return map
        }
    }
}
