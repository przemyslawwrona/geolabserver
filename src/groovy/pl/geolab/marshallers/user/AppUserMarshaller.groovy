package pl.geolab.marshallers.user

import grails.converters.JSON
import pl.geolab.marshallers.Marshaller
import pl.geolab.user.AppUser

class AppUserMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(AppUser) { AppUser appUser ->
            Map<String, String> map = [:]
            map['id'] = appUser.id
            map['username'] = appUser.username
            map['email'] = appUser.email
            map['userData'] = appUser.userData
            map['admin'] = appUser.isAdmin()
            return map
        }
    }
}
