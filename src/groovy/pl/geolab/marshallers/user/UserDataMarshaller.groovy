package pl.geolab.marshallers.user

import grails.converters.JSON
import pl.geolab.marshallers.Marshaller
import pl.geolab.user.UserData

class UserDataMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(UserData) { UserData userData ->
            Map<String, String> map = [:]
            map['id'] = userData.id
            map['dateCreated'] = userData.dateCreated
            map['lastUpdated'] = userData.lastUpdated
            map['name'] = userData.name
            map['config'] = userData.appConfig
            return map
        }
    }
}
