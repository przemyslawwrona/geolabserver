package pl.geolab.marshallers

class ObjectMarshallers {
    List<Marshaller> marshallers = []

    void register() {
        marshallers.each { it.register() }
    }
}
