package pl.geolab.marshallers

import grails.converters.JSON
import pl.geolab.AppConfig

class AppConfigMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(AppConfig) { AppConfig appConfig ->
            Map<String, String> map = [:]
            map['id'] = appConfig.id
            map['dateCreated'] = appConfig.dateCreated
            map['lastUpdated'] = appConfig.lastUpdated

            map['gradient'] = appConfig.gradient.type
            map['luni'] = appConfig.luni.type
            map['pressure'] = appConfig.pressure.type
            map['dryft'] = appConfig.dryft.type

            return map
        }
    }
}
