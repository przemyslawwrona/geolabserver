package pl.geolab.utils

import pl.codino.lib.baseapp.auth.AppRole
import pl.codino.lib.baseapp.auth.AppUserRole
import pl.codino.lib.baseapp.utils.DBBaseUtil
import pl.geolab.AppConfig
import pl.geolab.AppProject
import pl.geolab.user.AppUser
import pl.geolab.user.UserData

class DBUtil extends DBBaseUtil {

    public void initDB() {
        if (isFirstRun()) {
            initInserts()
            insertTestData()
        }
    }

    private static void initInserts() {
        insertAdmin()
        insertUser()
    }

    private static void insertAdmin() {
        AppUser admin = new AppUser(username: 'admin', password: 'password', email: 'tadeuszkadziej@wp.pl', hasConnectionConfiguration: false, userData: new UserData(name: 'Admin', appConfig: new AppConfig())).save(flush: true)
        AppUserRole.create(admin, AppRole.admin, true)
        admin.save(flush: true)
    }

    private static void insertUser() {
        AppUser user = new AppUser(username: 'user', password: 'password', email: 'user@wp.pl', hasConnectionConfiguration: false, userData: new UserData(name: 'User', appConfig: new AppConfig())).save(flush: true)
        AppUserRole.create(user, AppRole.user, true)
        user.save(flush: true)
    }

    private static void insertTestData() {
        insertProjects()
    }

    private static void insertProjects() {
        AppUser appUser = AppUser.findByUsername("user")

        new AppProject(title: "Pomiar osnowy", localization: "Warszawa/Kabaty", description: "Pomiar osnowy grawimetrycznej na terenie województwa mazowieckiego na zlecenie GUGiK", userData: appUser.userData).save(flush: true)
        new AppProject(title: "", localization: "Chmielnik", description: "", userData: appUser.userData).save(flush: true)
        new AppProject(title: "", localization: "Katowice", description: "", userData: appUser.userData).save(flush: true)
    }

}
