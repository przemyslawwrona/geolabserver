package pl.geolab.utils

import geo.gravimetry.Observation
import geo.gravimetry.correction.Correction
import geo.gravimetry.correction.CorrectionObservation
import geo.gravimetry.dryft.Dryft
import geo.gravimetry.gradient.Gradient
import geo.gravimetry.gradient.GradientStandard
import geo.gravimetry.gravimeter.Gravimeter
import geo.gravimetry.luni.Tide
import geo.gravimetry.luni.TideLongman
import geo.gravimetry.luni.TideWenzel
import geo.gravimetry.report.Report
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.joda.time.DateTime

class GravBuilder {
    private static final int NUMBER_OF_COLUMN = 3

    public static Report buildReport(JSONObject calibrationJSON, JSONObject correctionsJSON, JSONObject reportJSON) {
        Gravimeter gravimeter = buildGravimeterFromJSON(calibrationJSON)
        Correction poprawka = buildCorrectionsFromJSON(correctionsJSON)
        Integer dryft = buildDriftFromJSON(correctionsJSON)
        List<Observation> observations = builtObservationsFromJSON(reportJSON)

        return Report.build(gravimeter, observations, poprawka, dryft)
    }

    public static Correction buildCorrectionsFromJSON(JSONObject correctionsJSON) {
        Correction correction = new CorrectionObservation()
        correction = addGradient(correction, correctionsJSON)
        correction = addLuni(correction, correctionsJSON)
        correction = addPressure(correction, correctionsJSON)
        return correction
    }

    private static Correction addPressure(Correction correction, JSONObject corrections) {
        if (corrections.pressure) {
//            int pressure = Integer.parseInt(corrections.pressure)
        }
        return correction
    }

    private static Correction addLuni(Correction correction, JSONObject corrections) {
        if (corrections.luni) {
            int luni = Integer.parseInt((String) corrections.luni)
            switch (luni) {
                case Tide.LONGMAN:
                    correction = new TideLongman(correction);
                    break;
                case Tide.WENZEL:
                    correction = new TideWenzel(correction);
                    break;
            }
        }
        return correction
    }

    private static Correction addGradient(Correction correction, JSONObject corrections) {
        if (corrections.gradient) {
            int gradient = Integer.parseInt((String) corrections.gradient)
            switch (gradient) {
                case Gradient.GRADIENT_STANDARD:
                    correction = new GradientStandard(correction);
                    break;
            }
        }
        return correction
    }

    public static Gravimeter buildGravimeterFromJSON(JSONObject gravimeter) {
        JSONArray tableJSON = (JSONArray) gravimeter['table']
        double constant = gravimeter['constant'] ? Double.parseDouble((String) gravimeter['constant']) : 1.0D

        if (!tableJSON) { return new Gravimeter(constant) }

        double[][] tablicareferencyjna = new double[tableJSON.length()][NUMBER_OF_COLUMN]
        for (int i = 0; i < tableJSON.length(); i++) {
            tablicareferencyjna[i] = parseLineOfGravimeter((JSONArray) tableJSON.get(i))
        }
        return new Gravimeter(constant, tablicareferencyjna)
    }

    private static double[] parseLineOfGravimeter(JSONArray lineJSON) {
        double[] line = new double[NUMBER_OF_COLUMN];
        for (int i = 0; i < NUMBER_OF_COLUMN; i++) {
            line[i] = Double.parseDouble((String) lineJSON[i])
        }
        return line;
    }

    public static List<Observation> builtObservationsFromJSON(JSONObject report) {
        def observations = [];
        for (int i = 0; i < report.observations.length(); i++) {
            JSONObject o = (JSONObject) report.observations[i]

            observations.add(new Observation(String.valueOf(o.name),
                    new DateTime((String) o.date),
                    Double.parseDouble((String) o.value),
                    Double.parseDouble((String) o.instrumentHeight),
                    Double.parseDouble((String) o.gradient)))
        }
        return observations
    }

    public static int buildDriftFromJSON(JSONObject correctionsJSON) {
        return correctionsJSON.drift ? Integer.parseInt(correctionsJSON.drift) : Dryft.NONE;
    }
}
