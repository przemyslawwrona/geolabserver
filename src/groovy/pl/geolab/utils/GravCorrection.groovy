package pl.geolab.utils

class GravCorrection {
    public static enum Gradient {
        NONE(0),
        GRADIENT_STANDARD(1)

        int type

        public Gradient(int type) {
            this.type = type
        }
    }

    public static enum Luni {
        NONE(0),
        LONGMAN(1),
        WENZEL(2)

        int type

        public Luni(int type) {
            this.type = type
        }
    }

    public static enum Pressure {
        NONE(0),
        PRESSURE_STANDARD(1)

        int type

        public Pressure(int type) {
            this.type = type
        }
    }

    public enum Dryft {
        NONE(0),
        LINIOWY(1),
        LINIOWY_WAZONY(2)

        int type

        public Dryft(int type) {
            this.type = type
        }
    }
}
