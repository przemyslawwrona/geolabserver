package pl.geolab

import geo.gravimetry.report.Report

class AppReport {
    Long id
    Date dateCreated
    Date lastUpdated

    static belongsTo = [project: AppProject]

    static mapping = {
        id generator: 'identity', params: [sequence: 'grav_report_seq']
    }

    static constraints = {
        project nullable: true
    }

    AppReport fillData(Report report) {
        return this;
    }
}
