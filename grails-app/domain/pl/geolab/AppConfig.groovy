package pl.geolab

import pl.geolab.user.UserData
import pl.geolab.utils.GravCorrection

class AppConfig {
    Long id
    Date dateCreated
    Date lastUpdated

    GravCorrection.Gradient gradient
    GravCorrection.Luni luni
    GravCorrection.Pressure pressure
    GravCorrection.Dryft dryft

    public AppConfig() {
        this.gradient = GravCorrection.Gradient.GRADIENT_STANDARD
        this.luni = GravCorrection.Luni.LONGMAN
        this.pressure = GravCorrection.Pressure.NONE
        this.dryft = GravCorrection.Dryft.LINIOWY_WAZONY

    }

    static belongsTo = [userData: UserData]

    static constraints = {
    }

    static mapping = {
        id generator: 'identity', params: [sequence: 'project_seq']
    }
}
