package pl.geolab.user

import org.codehaus.groovy.grails.web.json.JSONObject
import pl.geolab.AppProject
import pl.geolab.AppConfig

class UserData {
    Long id
    Date dateCreated
    Date lastUpdated
    String name
    AppConfig appConfig

    static belongsTo = [appUser: AppUser]

    static hasMany = [projects: AppProject]

    static mapping = {
        id generator: 'identity', params: [sequence: 'user_data_seq']
    }

    static constraints = {
        name blank: true, nullable: true
        appConfig nullable: true
    }

    UserData fillData(JSONObject userDataJson) {
        name = userDataJson.name
        return this
    }
}
