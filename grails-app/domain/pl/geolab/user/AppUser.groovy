package pl.geolab.user

import org.codehaus.groovy.grails.web.json.JSONObject
import pl.codino.lib.baseapp.auth.AppUserBase

class AppUser extends AppUserBase {

    UserData userData

    static constraints = {
    }

    @Override
    AppUserBase fillData(JSONObject userJson) {
        AppUserBase appuser = super.fillData(userJson)
        return appuser
    }
}
