package pl.geolab

import org.codehaus.groovy.grails.web.json.JSONObject
import pl.geolab.user.UserData

class AppProject {
    Long id
    Date dateCreated
    Date lastUpdated

    String title
    String localization
    String description

    static hasMany = [reports: AppReport]

    static belongsTo = [userData: UserData]

    static mapping = {
        id generator: 'identity', params: [sequence: 'project_seq']
    }

    static constraints = {
    }

    AppProject fillData(JSONObject project) {
        this.title = project.title
        this.localization = project.localization
        this.description = project.description
        return this
    }

}
