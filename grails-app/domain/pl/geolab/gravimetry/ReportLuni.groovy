package pl.geolab.gravimetry

import org.codehaus.groovy.grails.web.json.JSONObject
import pl.geolab.AppProject
import pl.geolab.AppReport

class ReportLuni extends AppReport {
    Date startDate
    String positionJSON
    int luniCorrection
    int interval
    int numberOfLuniCorrection

    static constraints = {
    }

    ReportLuni fillData(AppProject project, Date startDate, JSONObject position, int luniCorrection, int interval, int numberOfLuniCorrection){
        this.project = project
        this.startDate = startDate
        this.positionJSON = position.toString()
        this.luniCorrection = luniCorrection
        this.interval = interval
        this.numberOfLuniCorrection = numberOfLuniCorrection
        return this
    }
}
