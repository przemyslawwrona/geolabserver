package pl.geolab.gravimetry

import org.codehaus.groovy.grails.web.json.JSONObject
import pl.geolab.AppProject
import pl.geolab.AppReport

class ReportGrav extends AppReport {
    byte[] calibrationJSON
    byte[] correctionsJSON
    byte[] observationsJSON

    static constraints = {
        calibrationJSON nullable: true
        correctionsJSON nullable: true
        observationsJSON nullable: true
    }


    ReportGrav fillData(AppProject project, JSONObject calibrationJSON, JSONObject correctionsJSON, JSONObject observationsJSON) {
        this.project = project
        this.calibrationJSON = calibrationJSON.toString().bytes
        this.correctionsJSON = correctionsJSON.toString().bytes
        this.observationsJSON = observationsJSON.toString().bytes
        return this
    }
}
