package pl.geolab

import grails.transaction.Transactional
import pl.codino.lib.baseapp.utils.Status

@Transactional
class ReportService {

    /**
     *
     * @param projectId
     * @return
     */
    Collection<AppReport> getReports(Long projectId) {
        AppProject project = AppProject.findById(projectId)
        return project?.reports
    }

    def getReport(Long projectId, Long reportId) {
        //TODO Implement getReport
    }

    Status  removeReport(Long projectId, Long reportId) {
        AppReport appReport = AppReport.findById(reportId)
        appReport.delete(flush: true)
        Status.getSuccess(appReport.id)
    }
}
