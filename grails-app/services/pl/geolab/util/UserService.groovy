package pl.geolab.util

import grails.transaction.Transactional
import org.springframework.security.access.prepost.PreAuthorize
import pl.codino.lib.baseapp.utils.Status
import pl.codino.lib.baseapp.utils.service.GetUserDataAbility
import pl.geolab.user.AppUser
import pl.geolab.user.UserData

@Transactional
class UserService implements GetUserDataAbility<AppUser, UserData> {

    @PreAuthorize('@validationService.canModifyUser(#userId)')
    AppUser getUser(Long userId) {
        return AppUser.findById(userId)
    }

    def getLoggedUser() {
        AppUser loggedUser = currentUser
        return loggedUser
    }

    def deleteUserAccount() {
        AppUser loggedUser = currentUser
        loggedUser.enabled = false
        loggedUser.save(flush: true)
        return Status.getSuccess()
    }
}
