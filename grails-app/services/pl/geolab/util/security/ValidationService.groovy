package pl.geolab.util.security

import grails.transaction.Transactional
import pl.codino.lib.baseapp.utils.service.GetUserDataAbility
import pl.geolab.user.AppUser
import pl.geolab.user.UserData

@Transactional
class ValidationService implements GetUserDataAbility<AppUser, UserData> {

    def isCurrentUserAdmin() {
        AppUser user = getCurrentUser()
        return (user && user.isAdmin())
    }

    def canModifyUser(Long userId) {
        AppUser user = getCurrentUser()
        if(user && user.id == userId) {
            return true
        }
        return isCurrentUserAdmin()
    }
}
