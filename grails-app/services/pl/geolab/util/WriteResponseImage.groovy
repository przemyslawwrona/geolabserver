package pl.geolab.util

import javax.servlet.http.HttpServletResponse

/**
 * Created by jg on 27.03.15.
 */
trait WriteResponseImage {

    def writeResponseImage(HttpServletResponse response, byte[] img) {
        if (img == null) {
            response.status = 404
        } else {
            def outputStream = null
            try {
                response.setHeader('Content-length', img.length.toString())
                response.contentType = "image/png"
                outputStream = response.outputStream
                outputStream << img
            } catch (IOException ioe) {
                //can't send to the browser
            } finally {
                try {
                    outputStream?.close()
                } catch (IOException ioe) {
                    //can't close
                }
            }
        }
    }
}