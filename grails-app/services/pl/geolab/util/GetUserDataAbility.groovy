package pl.geolab.util

import grails.plugin.springsecurity.SpringSecurityService
import pl.codino.lib.baseapp.auth.AppUserBase

/**
 * Created by jg on 27.03.15.
 */
trait GetUserDataAbility<T extends AppUserBase, U> {
    SpringSecurityService springSecurityService

    T getCurrentUser() {
        return springSecurityService.currentUser
    }

    U getCurrentUserData() {
        return getCurrentUser().userData
    }
}