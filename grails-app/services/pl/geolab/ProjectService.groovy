package pl.geolab

import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONObject
import pl.codino.lib.baseapp.utils.Status
import pl.codino.lib.baseapp.utils.service.GetUserDataAbility
import pl.geolab.user.AppUser
import pl.geolab.user.UserData

@Transactional
class ProjectService implements GetUserDataAbility<AppUser, UserData> {

    /**
     * Create empty project
     * @param projectJSON
     * @return success if an object has no errors, otherwise error
     */
    Status createProject(JSONObject projectJSON) {
        AppProject project = new AppProject().fillData(projectJSON)
        project.userData = getCurrentUserData()
        project.validate()
        if (project.hasErrors()) {
            return Status.getError()
        }
        project.save(flush: true)
        return Status.getSuccess(project.id)
    }

    /**
     * @param projectId id project
     * @return  saved user's project
     */
    AppProject getProject(Long projectId) {
        return AppProject.findById(projectId)
    }

    /**
     * Returns a list of projects that belong to the user
     * @return collection of project
     */
    Collection<AppProject> getAll() {
        return getCurrentUserData().projects
    }

    /**
     * Delete the project if it belongs to the logged in user
     * @param projectId id project
     * @return success if an object has no errors, otherwise error
     */
    Status deleteProject(Long projectId) {
        AppProject project = AppProject.findById(projectId)
        project.delete(flush: true)
        return Status.getSuccess()
    }

    /**
     * Update project with specific id
     * @param projectId
     * @param projectJSON
     * @return success if an object has no errors, otherwise error
     */
    Status updateProject(Long projectId, JSONObject projectJSON) {
        AppProject project = AppProject.findById(projectId)
        project.fillData(projectJSON)
        project.validate()
        if (project.hasErrors()) {
            return Status.getError()
        }
        project.save(flush: true)
        return Status.getSuccess();
    }
}
