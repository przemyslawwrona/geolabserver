package pl.geolab

import geo.gravimetry.report.Report
import grails.transaction.Transactional
import groovy.time.TimeCategory
import org.codehaus.groovy.grails.web.json.JSONObject
import pl.geolab.gravimetry.ReportGrav
import pl.geolab.gravimetry.ReportLuni
import pl.geolab.utils.GravBuilder
import pl.geolab.wrapper.LuniWrapper

@Transactional
class GravimeryService {
    public static final int NUMBER_OF_LUNI_CORRECTION = 50

    Report getReport(JSONObject calibrationJSON, JSONObject correctionsJSON, JSONObject reportJSON) {
        return report(calibrationJSON, correctionsJSON, reportJSON)
    }

    AppReport saveReport(Long projectId, JSONObject calibrationJSON, JSONObject correctionsJSON, JSONObject reportJSON) {
        AppProject project = AppProject.findById(projectId)
        ReportGrav reportGrav = new ReportGrav()
                .fillData(project, calibrationJSON, correctionsJSON, reportJSON)

        reportGrav.validate()
        if (!reportGrav.hasErrors()) {
            reportGrav.save(flush: true)
        }
        return reportGrav
    }

    Report getSavedReport(Long projectId, Long reportId) {
        ReportGrav reportGrav = ReportGrav.findById(reportId)
        return getReport(new JSONObject(new String(reportGrav.calibrationJSON)),
                new JSONObject(new String(reportGrav.correctionsJSON)),
                new JSONObject(new String(reportGrav.observationsJSON)))
    }

    List<LuniWrapper> getLuni(Date date, JSONObject position, int interval, int luniCorrection) {
        List<LuniWrapper> luniWrappers = new ArrayList<LuniWrapper>(NUMBER_OF_LUNI_CORRECTION)
        for (int i = 0; i < NUMBER_OF_LUNI_CORRECTION; i++) {
            //TODO analize luni
            luniWrappers.add(new LuniWrapper(date: date, value: 0.1256))
            use(TimeCategory) { date += interval.seconds }
        }
        return luniWrappers;
    }

    AppReport saveLuni(Long projectId, JSONObject position, Date date, int luniCorrection, int interval) {
        AppProject project = AppProject.findById(projectId)
        ReportLuni reportLuni = new ReportLuni()
                .fillData(project, date, position, luniCorrection, interval, NUMBER_OF_LUNI_CORRECTION)

        reportLuni.validate()
        if (!reportLuni.hasErrors()) {
            reportLuni.save(flush: true)
        }
        return reportLuni
    }

    List<LuniWrapper> getSavedLuni(Long projectId, Long reportId) {
        ReportLuni reportLuni = ReportLuni.findById(reportId)
        return getLuni(reportLuni.startDate, new JSONObject(reportLuni.positionJSON), reportLuni.interval, reportLuni.luniCorrection)
    }

    private static Report report(JSONObject calibrationJSON, JSONObject correctionsJSON, JSONObject reportJSON) {
        Report report = GravBuilder.buildReport(calibrationJSON, correctionsJSON, reportJSON)
        report.obliczDziennik();
        return report
    }
}
