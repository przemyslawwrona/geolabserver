package pl.geolab

import grails.converters.JSON

class ReportController {

    ReportService reportService

    def getReports(Long projectId) {
        render reportService.getReports(projectId) as JSON
    }

    def getReport(Long projectId, Long reportId) {
        render reportService.getReport(projectId, reportId) as JSON
    }

    def removeReport(Long projectId, Long reportId) {
        render reportService.removeReport(projectId, reportId) as JSON
    }
}
