package pl.geolab

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class GravimetryController {
    GravimeryService gravimeryService

    def getReport() {
        JSONObject calibration = new JSONObject((String) params.calibration)
        JSONObject corrections = new JSONObject((String) params.corrections)
        JSONObject report = new JSONObject((String) params.report)
        render gravimeryService.getReport(calibration, corrections, report) as JSON
    }

    def saveReport(Long projectId) {
        JSONObject calibration = new JSONObject((String) request.getJSON()['calibration'])
        JSONObject corrections = new JSONObject((String) request.getJSON()['corrections'])
        JSONObject report = new JSONObject((String) request.getJSON()['report'])
        render gravimeryService.saveReport(projectId, calibration, corrections, report) as JSON
    }

    def getSavedReport(Long projectId, Long reportId) {
        render gravimeryService.getSavedReport(projectId, reportId) as JSON
    }

    def getLuni() {
        JSONObject position = new JSONObject(params.position)
        Date date = new Date()
        int interval = Integer.parseInt(params.interval)
        int luniCorrection = Integer.parseInt(params.luniCorrection)
        render gravimeryService.getLuni(date, position, interval, luniCorrection) as JSON
    }

    def saveLuni(Long projectId){
        JSONObject position = request.getJSON().position
        Date date = new Date()
        int luniCorrection = Integer.parseInt((String) request.getJSON()['luniCorrection'])
        int interval = Integer.parseInt((String) request.getJSON()['interval'])
        render gravimeryService.saveLuni(projectId, position, date, luniCorrection, interval) as JSON
    }

    def getSavedLuni(Long projectId, Long reportId) {
        render gravimeryService.getSavedLuni(projectId, reportId) as JSON
    }

    def renderReportInPdfFormat() {
    }
}
