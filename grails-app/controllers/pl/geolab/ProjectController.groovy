package pl.geolab

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class ProjectController {

    ProjectService projectService

    def createProject() {
       JSONObject project = request.getJSON().project
        render projectService.createProject(project) as JSON
    }

    def getProject(Long projectId) {
        render projectService.getProject(projectId) as JSON
    }

    def getAll() {
        render projectService.getAll() as JSON
    }

    def deleteProject(Long projectId) {
        render projectService.deleteProject(projectId) as JSON
    }

    def updateProject(Long projectId) {
        JSONObject projectJSON = request.getJSON().project
        render projectService.updateProject(projectId, projectJSON) as JSON
    }
}
