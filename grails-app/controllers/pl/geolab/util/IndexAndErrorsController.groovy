package pl.geolab.util

import org.codehaus.groovy.grails.core.io.ResourceLocator
import org.springframework.core.io.Resource

class IndexAndErrorsController {

    ResourceLocator grailsResourceLocator
    def grailsApplication

    def index() {
        if (request.getHeader("host").startsWith('www.')) {
            redirect(uri: grailsApplication.config.grails.serverURL)
        } else {
            response.setStatus(200)
            Resource image = grailsResourceLocator.findResourceForURI('index.html')
            render file: image.inputStream, contentType: 'text/html'
        }
    }

    def notFound() {
        index()
    }
}
