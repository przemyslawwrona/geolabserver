package pl.geolab.util

import grails.converters.JSON

class UsersController {

    UserService userService

    def getCurrentUser() {
        render userService.getLoggedUser() as JSON
    }

    def deleteUserAccount() {
        render userService.deleteUserAccount() as JSON
    }
}
