class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "indexAndErrors", action: "index")
        "404"(controller: "indexAndErrors", action: "notFound")

        "500"(view: '/error')

        "/api/secure/loggedUser"(controller: "users", parseRequest: true) { action = [GET: "getCurrentUser", DELETE: "deleteUserAccount"] }
        "/api/secure/password/change"(controller: "baseUsers", parseRequest: true) { action = [POST: "changePassword"] }


        "/api/secure/project"(controller: "project", parseRequest: true) { action = [GET: "getAll",  POST: "createProject"] }
        "/api/secure/project/$projectId"(controller: "project", parseRequest: true) { action = [GET: "getProject", DELETE: "deleteProject", POST: "updateProject"] }

        /** Gravimetry */
        "/api/secure/project/$projectId/reports"(controller: "report", parseRequest: true) { action = [GET: "getReports"] }

        "/api/secure/project/$projectId/gravimetry/luni"(controller: "gravimetry", parseRequest: true) { action = [POST: "saveLuni"] }
        "/api/secure/project/$projectId/gravimetry/report"(controller: "gravimetry", parseRequest: true) { action = [POST: "saveReport"] }

        "/api/secure/project/$projectId/reports/$reportId/luni"(controller: "gravimetry", parseRequest: true) { action = [GET: "getSavedLuni"] }
        "/api/secure/project/$projectId/reports/$reportId/gravimetry"(controller: "gravimetry", parseRequest: true) { action = [GET: "getSavedReport"] }

        /** Open API */
        "/api/secure/reports/gravimetry"(controller: "gravimetry", parseRequest: true) { action = [GET: "getReport"] }
        "/api/secure/reports/luni"(controller: "gravimetry", parseRequest: true) { action = [GET: "getLuni"] }


    }
}

