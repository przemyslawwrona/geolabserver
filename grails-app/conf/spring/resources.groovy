import pl.geolab.marshallers.AppConfigMarshaller
import pl.geolab.marshallers.ObjectMarshallers
import pl.geolab.marshallers.gravimetry.GravDryftMarshaller
import pl.geolab.marshallers.gravimetry.GravObservationDryftMarshaller
import pl.geolab.marshallers.gravimetry.LuniWrapperMarshaller
import pl.geolab.marshallers.user.AppUserMarshaller
import pl.geolab.marshallers.project.ProjectMarshaller
import pl.geolab.marshallers.project.ReportMarshaller
import pl.geolab.marshallers.gravimetry.GravObserwationMarshaller

import pl.geolab.marshallers.gravimetry.GravReportMarshaller
import pl.geolab.marshallers.user.UserDataMarshaller

beans = {
    objectMarshallers(ObjectMarshallers) {
        marshallers = [
                new AppUserMarshaller(),
                new UserDataMarshaller(),
                new AppConfigMarshaller(),
                new ProjectMarshaller(),
                new GravReportMarshaller(),
                new GravObserwationMarshaller(),
                new GravDryftMarshaller(),
                new GravObservationDryftMarshaller(),
                new LuniWrapperMarshaller(),
                new ReportMarshaller()

        ]
    }
}
