import org.springframework.web.context.support.WebApplicationContextUtils
import pl.geolab.utils.DBUtil


class BootStrap {

    def init = { servletContext ->
        new DBUtil().initDB()
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        def springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        springContext.getBean( "objectMarshallers" ).register()
    }
    def destroy = {
    }
}
